#include <stdio.h>

/* 1990年1月1日 是星期一 */

char* name[] = {"日", "一", "二", "三", "四", "五", "六"};

int isvalid(int year, int month, int day)
{
    if (year < 1583)
    {
        return 0;
    }

    if (month < 1 || month > 12)
    {
        return 0;
    }

    if (day < 1 || day > 31)
    {
        return 0;
    }

    return 1;
}

int weekday(int year, int month, int day)
{
    if (!isvalid(year, month, day))
    {
        return -1;
    }

    //使用蔡勒公式计算
    int c = year/100;
    int y = year%100;
    int m = month;
    if (month < 3)
    {
        y--;
        m += 12;
    }

    int sum = c/4 - 2*c + y + y/4 + 13*(m+1)/5 + day - 1;

    return ((sum%7)+7)%7;
}

int main()
{
    int year, month, day, week;

    scanf("%d/%d/%d", &year, &month, &day);

    week = weekday(year, month, day);
    
    printf("%d年%d月%d日是星期%s\n", year, month, day, name[week]);

    return 0;
}
